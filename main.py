import os
import json
import settings

from zenoss import Zenoss
from slacker import Slacker

zenoss = Zenoss(settings.ZENOSS_URL, settings.ZENOSS_USERNAMAE, settings.ZENOSS_PASS)
slack = Slacker(settings.SLACK_API_TOKEN)

def device_compare(new_device, old_device):
  new_device_keys = set(new_device.keys())
  old_device_keys = set(old_device.keys())

  intersect_keys = new_device_keys.intersection(old_device_keys)
  added = new_device_keys - old_device_keys
  removed = old_device_keys - new_device_keys
  modified = {o : (new_device[o], old_device[o]) for o in intersect_keys if new_device[o] != old_device[o]}
  same = set(o for o in intersect_keys if new_device[o] == old_device[o])
  return added, removed, modified, same

def store_compare_json(new_devices):
    """
    Compare existing records with new

    """

    new_devices_uids = []
    old_devices_renamed_deleted = []

    for new_device in new_devices:
        new_devices_uids.append(new_device.get('uid'))

    with open('devices.json') as devices:
        old_devices = json.load(devices)

        for old_device in old_devices:
            if old_device.get('uid') in new_devices_uids:
                """
                changed
                """

                new_device = [d for d in new_devices if d.get('uid')==old_device.get('uid')]
                added, removed, modified, same = device_compare(new_device.pop(), old_device)
                message = 'device {0} has been modified'.format(old_device.get('uid'))
                slack.chat.post_message('#general', message)
                new_devices_uids.remove(old_device.get('uid'))
            else:
                old_devices_renamed_deleted.append(old_device.get('uid'))

        """
        renamed/deleted
        """
        for old_device in old_devices_renamed_deleted:
            message = 'device {0} has been renamed or deleted'.format(old_device)
            slack.chat.post_message('#general', message)

        """
        new devices
        """
        for new_device in new_devices_uids:
            message = 'device {0} has been added'.format(new_device)
            slack.chat.post_message('#general', message)

    with open('devices.json', 'w+') as fp:
        json.dump(new_devices, fp)
        fp.close()



def process():
    """
    1. Get all devices, record everything on json file for first time.
    2. If json file exists, look for differences
    """

    devices = zenoss.get_devices()['devices']

    if os.path.isfile('devices.json'):
        store_compare_json(devices)
    else:
        with open('devices.json', 'w+') as fp:
            json.dump(devices, fp)
            fp.close()


if __name__ == "__main__":
    process()

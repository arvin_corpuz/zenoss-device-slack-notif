# zenoss-slack

Simple Python Script giving slack notifications for every device property change in Zenoss

##  Minimal Set-up
Clone project to /z (assuming z is the development directory)

Edit /z/zenoss-device-slack-notif/settings.py
```
  ZENOSS_USERNAMAE = '<username>'
  ZENOSS_PASS = '<password>'
  ZENOSS_URL = 'http://<ip>:<port>/'

  SLACK_API_TOKEN = '<api-slack-token>'
```

Edit crontab
```
  */5 * * * * zenoss zendmd --script=/z/zenoss-device-slack-notif/rename.py
  */3 * * * * zenoss python /z/zenoss-device-slack-notif/main.py
```

Output
![ScreenShot](https://storage.googleapis.com/cascadeo-cloud.appspot.com/zenoss-slack-output.png)
